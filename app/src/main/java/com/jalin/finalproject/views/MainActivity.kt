package com.jalin.finalproject.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jalin.finalproject.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}